package pointJack.pointjack.gui.listeners;

import pointJack.game.Card;
import java.util.logging.Level;
import java.util.logging.Logger;
import pointJack.pointjack.gui.PointGUI;
import pointJack.pointjack.client.PointClient;
import pointJack.game.Logic;
import pointJack.game.Player;
import pointJack.game.PlayerDeck;
import pointJack.gui.GUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class TurnMadeListener implements ActionListener 
{
    private Logic logic;
    private PointGUI gui;
    private PointClient client;

    public TurnMadeListener(GUI gui) 
    {
	this.gui = (PointGUI) gui;
	logic = this.gui.getClient().getLogic();
	client = (PointClient) gui.getClient();
    }

    @Override
    public void actionPerformed(ActionEvent e) 
    {
	Player p = logic.getUserPlayer();
	
	// Operation to peform when the Undo GUI button is selected.
	if (e.getActionCommand().equals("Undo"))
	{
	    PlayerDeck cards = p.getActiveCards();
	    for (Card c : cards)
	    {
		logic.getUserPlayer().getDeck().replaceEmptyCard(c);
	    }
	    p.getActiveCards().clear();
	    gui.getCardsPanel().refresh();
	}
	else
	{
	    // Compute the score then send it.
	    int total = logic.processTurn();
	    logic.updateScore(p, total);
	    client.getState().put("points", total);
	    client.getState().put("player", p.getID());	    
	    client.getDatabase().query("awardpoints");
	    
	    // Remove the played cards.
	    PlayerDeck deck = p.getActiveCards();
	    client.getState().put("cards", deck.getDeck());
	    client.getDatabase().query("discardplayedcards");	    
	    
	    // Take the new cards.
	    client.getState().put("num_cards_to_replace", deck.size());
	    ArrayList<Card> cards = (ArrayList<Card>) client.getDatabase().query("getreplacementcards");
	    
	    for (Card c : cards)
	    {
		try
		{
		    logic.getUserPlayer().getDeck().add(c);
		} 
		catch (Exception ex)
		{
		    Logger.getLogger(TurnMadeListener.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	    gui.getDiscardedPanel().getCards().clear();
	    
	    // Set discarded cards from GUI.
	    for (Card c : p.getActiveCards())
	    {
		gui.getDiscardedPanel().setDiscarded(c);
	    }
	    
	    gui.setVisible(true);
	    // Clear the played cards deck.
	    logic.getUserPlayer().getActiveCards().clear();

	    int player = (Integer) client.getDatabase().query("nextplayer");
	    client.getState().put("player", player);	
	}
	
	// GUI for played cards will now be empty, naturally.
	gui.getPlayedCardsPanel().reset();
	gui.setVisible(true);
    }
}
