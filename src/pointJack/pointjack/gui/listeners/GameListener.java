package pointJack.pointjack.gui.listeners;

import pointJack.pointjack.client.PointClient;
import pointJack.game.Logic;
import pointJack.gui.GUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * The GameListener is what helps keep the program running.
 *	The class is invoked periodically via a timer, and refreshes
 *	gui and ensures the appropriate player is playing.
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class GameListener implements ActionListener
{
    private GUI gui;
    private PointClient client;
    private Logic logic;

    public GameListener(GUI gui) 
    {
	this.gui = gui;
	this.client = (PointClient) gui.getClient();
	logic = gui.getClient().getLogic();
    }

    /*
     * Handler for the game listener. Refreshes the GUI. Sets the current player.
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {	
	HashMap<String, Object> state = client.getState();

	int id = (Integer) client.getState().get("player");
	logic.setCurrentPlayer(id);
	gui.refresh();
    }
}
