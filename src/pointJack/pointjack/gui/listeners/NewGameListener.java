package pointJack.pointjack.gui.listeners;

import pointJack.pointjack.client.PointClient;
import pointJack.pointjack.gui.PointGUI;
import pointJack.game.Logic;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class NewGameListener implements ActionListener
{
    private PointGUI gui;

    public NewGameListener(JFrame gui)
    {
	this.gui = (PointGUI) gui;
    }

    @Override
    public void actionPerformed(ActionEvent e) 
    {
	Logic l = gui.getClient().getLogic();

	l.restart();

	PointClient pc = (PointClient) gui.getClient();
	
	pc.newGame();
	pc.restore();
	gui.setNewGame(false);
    }
    
}
