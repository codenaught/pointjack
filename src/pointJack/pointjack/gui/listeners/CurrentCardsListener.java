
package pointJack.pointjack.gui.listeners;

import pointJack.game.Card;
import pointJack.pointjack.gui.PointGUI;
import pointJack.game.Logic;
import pointJack.gui.GUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * Controls what happens when one of the current cards in the player's deck
 *	is selected. For the handler for hitting the actual submit button,
 *	see TurnMadeListener.
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class CurrentCardsListener implements ActionListener
{
    private JFrame gui;
    private Logic logic;
    
    public CurrentCardsListener(GUI gui)
    {
	this.gui = (PointGUI) gui;
	logic = gui.getClient().getLogic();
    }

    /*
     * Handler method for the class. Operates when any card is selected in
     *	    current card panel.
     */
    @Override
    public void actionPerformed(ActionEvent e) 
    {	
	// Grab the proper card associated with the button.
	JButton button = (JButton) e.getSource();
	Card c = logic.getUserPlayer().getDeck().get((Integer)button.getClientProperty("id"));	
	
	if (c != null)
	{
	    ((PointGUI)gui).getPlayedCardsPanel().placeCard(c);

	    // For now...
	    logic.removeFromUserDeck(c);
	}
	// Force a refresh.
	((PointGUI)gui).refresh();
	gui.setVisible(true);
    }
}
