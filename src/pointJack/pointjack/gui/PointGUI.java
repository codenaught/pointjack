package pointJack.pointjack.gui;

import pointJack.client.Client;

import pointJack.game.Logic;

import pointJack.gui.CurrentCardsPanel;
import pointJack.gui.DiscardDeckPanel;
import pointJack.gui.GUI;
import pointJack.gui.GamePanel;
import pointJack.gui.PlayedCardsPanel;
import pointJack.gui.ScorePanel;

import pointJack.pointjack.gui.listeners.NewGameListener;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 * The heart of the GUI rests in PointGUI. This class acts as 
 *	a JFrame and handles the general layout for storing all of the contents
 *	for the program.
 * TODO: Introduce support for multi-threading.
 */
public class PointGUI extends JFrame implements GUI
{
    private static final long serialVersionUID = 1L;
    
    private Client pc;

    private JPanel rulesPanel;
    private GamePanel cardsPanel;
    private GamePanel playedCardsPanel;
    private JPanel newGamePanel;
    private GamePanel discardDeckPanel;
    private GamePanel scorePanel;
    private JTextArea rulesLabel;   

    public final String rulesText = "POINT JACK";

    private Logic logic;

    /*
     * TODO: SIMPLIFY.
     */
    public PointGUI(Client pc)
    {
	super();

	this.pc = pc;
	logic = pc.getLogic();
	
	// Configure the layout for this frame.
	this.getContentPane().setLayout(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	c.fill = GridBagConstraints.HORIZONTAL;
	c.anchor = GridBagConstraints.WEST;
	rulesPanel = new JPanel();

	// Background color and size.
	this.getContentPane().setBackground(new Color(0xccd9ff));
        this.setMinimumSize(new Dimension(1024, 768));
	
	// Panel used only when a game ends with a button to start a new one.
	newGamePanel = new JPanel();
	JButton newButton = new JButton("Start New Game");
	newButton.addActionListener(new NewGameListener(this));
	newGamePanel.add(newButton);
	
	// Construct all of the custom onject panels used for this project.
	cardsPanel = new CurrentCardsPanel(this);
	playedCardsPanel = new PlayedCardsPanel(this);
	discardDeckPanel = new DiscardDeckPanel(this);
	scorePanel = new ScorePanel(this);
	
	// Styling for the name element.
	c.ipady = 20;
	c.gridx = 0;
	c.gridy = 0;
	c.gridwidth = 5;
	rulesLabel = new JTextArea(String.format(rulesText, "?"));
	rulesPanel.add(rulesLabel);
	
	// TODO: Should use another method to prevent width from changing.
	// Operations controlling the rulesLabel (just contains name for now).
	rulesLabel.setLineWrap(true);
	rulesLabel.setColumns(40);
	rulesLabel.setEditable(false);
	rulesLabel.setBackground(new Color(0x70a1c3));
	rulesLabel.setForeground(Color.black);
	Font font = new Font("Serif", Font.BOLD, 20);
	rulesLabel.setFont(font);
	rulesPanel.setBorder(BorderFactory.createLineBorder(new Color(0x7388b2), 2));
	rulesPanel.setBackground(new Color(0x70a1c3));

	this.getContentPane().add(rulesPanel, c);

	// Styling for everything else
	c.gridy = 1;	
	c.gridheight = 1;

	// Add the scorePanel to our frame, column below rulesLabel.
	this.getContentPane().add(scorePanel, c);
	c.gridy = 2;
	
	// Add gamePanel.
	this.getContentPane().add(newGamePanel, c);
	newGamePanel.setVisible(false);
	c.gridy = 3;
	
	// discardDeckPanel
	this.getContentPane().add(discardDeckPanel, c);
	c.gridy = 4;
	
	// cardsPanel
	this.getContentPane().add(cardsPanel, c);
	c.gridy = 5;
	
	// playedCardsPanel at the very bottom.
	this.getContentPane().add(playedCardsPanel, c);
	
	
	// Get the size of the screen
	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

	// Determine the new location of the window
	int w = this.getSize().width;
	int h = this.getSize().height;
	int x = (dim.width-w)/2;
	int y = (dim.height-h)/2;

	// Move the window
	this.setLocation(x, y);
	
	this.setVisible(true);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    @Override
    public Client getClient() 
    {
	return pc;
    }
    
    public void start()
    {
	this.setVisible(true);
    }

    /* Invoked automatically by GameListener constantly, calls the
	    refresh method of each custom panel object. */
    @Override
    public void refresh() 
    {
	rulesLabel.setText(String.format(rulesText, logic.getPointsToWin()));
	scorePanel.refresh();
	discardDeckPanel.refresh();
	((CurrentCardsPanel)cardsPanel).refresh();
	playedCardsPanel.refresh();	
    }
    
    public CurrentCardsPanel getCardsPanel()
    {
	return (CurrentCardsPanel)cardsPanel;
    }
    
    public PlayedCardsPanel getPlayedCardsPanel()
    {
	return (PlayedCardsPanel) playedCardsPanel;
    }
    
    public DiscardDeckPanel getDiscardedPanel()
    {
	return (DiscardDeckPanel) discardDeckPanel;
    }
    
    public ScorePanel getScorePanel()
    {
	return (ScorePanel) scorePanel;
    }

    /*
     * After a game has been completed, the new game button will show up that
     *	    when clicked will setup a new game to play.
     */
    @Override
    public void setNewGame(boolean isNew) 
    {
	newGamePanel.setVisible(isNew);
	this.pack();
	this.repaint();
    }
}
