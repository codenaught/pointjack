package pointJack.pointjack.client;

import pointJack.game.Card;
import pointJack.game.Logic;
import pointJack.game.Player;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class PointDatabase 
{
	private final String DRIVER_NAME = "com.mysql.jdbc.Driver";

	private String dbUser;
	private String dbPass;
	private String dbPath;
	private Connection connection;
	private PointDatabaseQuery queries;
	private HashMap<String, Object> state;
	private Logic logic;

	public PointDatabase(String user, String pass, String path)
	{
		dbUser = user;
		dbPass = pass;
		dbPath = path;
		queries = null;
	}

	public void connect() throws SQLException
	{
		StringBuilder builder = new StringBuilder();
		builder.append("jdbc:mysql://");
		builder.append(dbPath);
		builder.append("?user=");
		builder.append(dbUser);
		builder.append("&password=");
		builder.append(dbPass);
		try
		{
			Class.forName(DRIVER_NAME);
			String connectionUrl = builder.toString();
			connection = DriverManager.getConnection(connectionUrl);
		
			queries = new PointDatabaseQuery(connection, state);
		}
		catch (ClassNotFoundException ex) 
		{
			Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE,
				null, ex);
			System.exit(1);
		}
	}

    // NOTE: Design pattern might be best to use a class for each operation.
    // Method has too much code in one place.
    private Object performTask(String action)
    {
	if (action.equals("newgame"))
	{
	    ArrayList<Card> cards = new ArrayList<Card>();
	    
	    int card_id = 1;
	    
	    for (int i = 1; i <= 14; i++)
	    {
		for (int j = 1; j <= 6; j++)
		{
		    cards.add(new Card(i, card_id));
		    card_id++;
		}
	    }
	    Collections.shuffle(cards);
	    int game = 0;
	    if (state.get("game") != null)
		game = (Integer) state.get("game");
	    
	    return queries.newGame(cards, game);
	}
	
	else if (action.equals("clear"))
	{
	    queries.clear();
	    return null;
	}

	else if (action.equals("getscores"))
	{
	    // Note: Code here needs some work. Just a temp "jack" job.
	    ArrayList<Integer> scores = queries.getScores();
	    int id = 0;
	    for (Integer score : scores)
	    {
		Player p = logic.getPlayers().get(id);
		p.getScore().set("pointjack", score);
		id++;
	    }

	    return null;
	}
	else if (action.equals("nextplayer"))
	{
	    int player = queries.nextPlayer(logic.getNumPlayers());
	    return player;
	}
	else if (action.equals("awardpoints"))
	{
	    double points = logic.getUserPlayer().getScore().get("pointjack");
	    queries.awardpoints(points);
	    return null;
	}
	else if (action.equals("addplayers"))
	{
	    queries.addPlayers(logic.getPlayers());
	    return null;
	}
	else if (action.equals("getcurrentplayer"))
	{
	    return queries.getCurrentPlayer();
	}
	else if (action.equals("dealcards"))
	{
	    int num_players = logic.getNumPlayers();
	    queries.dealCards(num_players);
	    return null;
	}
	// Restore a game based off the current values saved in the database.
	else if (action.equals("restoregame"))
	{
	    // Add the players.
	    ArrayList<Player> players = queries.restorePlayers();
	    
	    if (logic.getNumPlayers() == 0)
	    {
		for (Player p : players)
		{
		    logic.addPlayer(p);
		}
	    }
	    
	    int player_id = (Integer) state.get("user_player");
	    Player p = logic.getPlayers().get(player_id);
	    logic.setUserPlayer(p);

	    // Make sure their hand is empty...
	    logic.getUserPlayer().getDeck().clear();
	    
	    // Add the cards.
	    // Note using a setCards should probably be considered for efficency.
	    // Right now trying to avoid the dependency of using the Logic class inside of the Query
	    //	    class, hence not directly adding the cards there as might be expected.
	    ArrayList<Card> cards = queries.restoreCards();

	    for (Card c : cards)
	    {
		try 
		{
		    logic.getUserPlayer().getDeck().add(c);
		} 
		catch (Exception ex)
		{
		    Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	    
	    return null;
	}
	// Simply return the id of the latest game.
	// Grossly over-simplified procedure: In real world usage there would be auth used
	//	to load the appropriate game, not neccesarily the last game in the db.
	else if (action.equals("getlatestgame"))
	{
	    return queries.getLatestGame();
	}
	else if (action.equals("discardplayedcards"))
	{
	    queries.discardCards();
	    return null;
	}
	else if (action.equals("getreplacementcards"))
	{
	    return queries.dealReplacementCards();
	}
	else
	{
	    return null;
	}
    }
    
    public Object query(String query)
    {
	//SQL query command
	Object sql = performTask(query);
	return sql;
    }

    public void setState(HashMap<String, Object> state) 
    {
	this.state = state;
    }

    public void setLogic(Logic logic) 
    {
	this.logic = logic;
    }
    
    public Logic getLogic()
    {
	return logic;
    }
}
