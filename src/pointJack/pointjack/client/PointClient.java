package pointJack.pointjack.client;

import pointJack.client.Client;
import pointJack.game.Logic;
import pointJack.game.Player;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class PointClient implements Client
{
    private String URL;
    private Logic logic;
    private int gameID;
    private int userID;
    private PointDatabase db;
    
    // Holds the current state of the object to be used for making http queries.
    private HashMap<String, Object> state;

    public int getGame() 
    {
	return gameID;	
    }
    
    public PointClient(String url)
    {
	URL = "http://localhost/game/script.php";
	gameID = 0;
	state = new HashMap<String, Object>();
    }
    
    public void setUserID(int id)
    {
	userID = id;
    }
    
    @Override
    public boolean newGame()
    {
	//state.put("action", "checklastgame");
	int latest = (Integer) db.query("getlatestgame");
	//String check = sendCommand();	
	boolean newgame = latest == 1;
	
	if (newgame)
	{
	    int id = (Integer) db.query("newgame");
	    gameID = id;
	    state.put("game", gameID);
	    
	    addPlayer(0);
	    addPlayer(1);

	    db.query("addplayers");
	    deal();
	}
	else
	{
	    gameID = latest;
	    state.put("game", gameID);
	}
	return newgame;
    }
    
    public void setGameID(int id)
    {
	gameID = id;
    }
    
    public void deal()
    {	
	db.query("dealcards");
    }
    
    @Override
    public String sendCommand()
    {
	String str = "";
	for(Entry<String, Object> entry : state.entrySet())
	{
	    String key = entry.getKey();
	    String value = (String) entry.getValue().toString();
	    
	    str += key + "=" + value + "&";
	}
	//System.out.println(str);
	return sendCommand(str);	
    }
    
    public String sendCommand(String ext)
    {        
	String URLToSend = URL + "?" + ext;
	StringBuilder builder = new StringBuilder();

	URL resource = null;

	try 
	{
	    resource = new URL(URLToSend);
	} 
	catch (MalformedURLException ex) 
	{
	    Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, null, ex);
	}
	InputStream st = null;

	try
	{
	   st = resource.openStream();
	   InputStreamReader isr = new InputStreamReader(st);
	   BufferedReader r = new BufferedReader(isr);

	   String s = r.readLine();
	   while (s != null)
	   {
		builder.append(s);
		s = r.readLine();
	   }
	   st.close();
	   isr.close();
	   r.close();
	} 
	catch (IOException ex) 
	{
	    Logger.getLogger(PointClient.class.getName()).log(Level.SEVERE, null, ex);
	}

	return builder.toString();
    }

    public void addPlayer(int id) 
    {
	String name;
	// Hack for now...
	if (id == 0)
	    name = "Jack";
	else
	    name = "Nancy";
	logic.addPlayer(new Player(id, name));
    }
    
    @Override
    public void restore()
    {
	BufferedReader br = null;
	int user_id = -1;

	try 
	{
	    FileInputStream stream = new FileInputStream(
	    	System.getProperty("user.home") + "/user.txt");
	    br = new BufferedReader(new InputStreamReader(
	    	new DataInputStream(stream)));
	}

	catch (FileNotFoundException ex) 
	{
	    Logger.getLogger(
	    	PointClient.class.getName()).log(Level.SEVERE, null, ex);
	}

	try 
	{
	    if (br != null)
	    {
		user_id = Integer.parseInt(br.readLine());
	    }
	}
	
	catch (IOException ex) 
	{
	    Logger.getLogger(
	    	PointClient.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	if (user_id == -1)
	{
	    System.exit(1);
	}	
	
	restore(user_id);	
    }

    public void restore(int user_id) 
    {
	Logger.getLogger(
		PointClient.class.getName()).log(Level.INFO, 
			"Restoring game ID " + gameID + " using player ID " 
				+ user_id, user_id);
	
	state.put("user_player", user_id);
	
	int player = (Integer) db.query("getcurrentplayer");
	state.put("player", player);
	
	db.query("restoregame");

	
	logic.setCurrentPlayer(player);
	logic.setPointsToWin(200);
    }
    
    @Override
    public String toString()
    {
	String s = logic.toString();
	return s;
    }

    void clearDB() 
    {
	state.put("action", "clear");
	sendCommand();
    }

    @Override
    public void setOver() 
    {
	state.put("player", logic.getNumPlayers());
	int player = (Integer) db.query("nextplayer");
	state.put("player", player);
    }
    
    @Override
    public void setLogic(Logic logic)
    {
	this.logic = logic;
    }

    @Override
    public HashMap<String, Object> getState() 
    {
	return state;
    }

    @Override
    public Logic getLogic() 
    {
	return logic;
    }

    @Override
    public void setDatabase(PointDatabase db) 
    {
	this.db = db;
    }

    @Override
    public PointDatabase getDatabase() 
    {
	return db;
    }
}
