package pointJack.pointjack.client;

import pointJack.game.Card;
import pointJack.game.Player;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class PointDatabaseQuery 
{
    private Connection connection;
    private HashMap<String, Object> state;
    
    public PointDatabaseQuery(Connection conn, HashMap<String, Object> state)

    {
	connection = conn;
	this.state = state;
    }
    
    public ArrayList<Integer> getScores()
    {
	StringBuilder sb = new StringBuilder();
	String sql = "SELECT player_id, player_name, score FROM player WHERE " 
		+ "game_id = " + state.get("game");
	ResultSet rs = performQuery(sql);
	ArrayList<Integer> scores = new ArrayList<Integer>();
	try 
	{
	    while (rs.next()) 
	    {
		int score = rs.getInt("score");
		scores.add(score);
	    }
	} 
	catch (SQLException ex) 
	{
	    Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE,
	   	null, ex);
	}

	return scores;
    }
    
    public void awardpoints(double points)
    {
	Integer game = (Integer) state.get("game");
	Integer player = (Integer) state.get("player");
	String sql = "UPDATE player SET score = " + points + " WHERE game_id =" 
		+ game + " AND player_id = " + player;
	this.performUpdate(sql);
    }
    
    public int nextPlayer(int rollover)
    {
	int player = (Integer) state.get("player") + 1;
	
	if (player == rollover)
	{
	    player = 0;
	}
	
	String sql = "UPDATE game SET status = " + player + " WHERE game_id = " 
		+ state.get("game");
	this.performUpdate(sql);
	
	return player;
    }
    
    public int getCurrentPlayer()
    {
	String sql = "SELECT status FROM game WHERE game_id = " 
		+ state.get("game") + " LIMIT 1;";	
	ResultSet rs = performQuery(sql);
	int player_id = 0;

	try 
	{
	    if (!rs.next())
	    {
		return 0;
	    }
	    player_id = rs.getInt("status");
	} 
	catch (SQLException ex) 
	{
	    Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE, 
	    	null, ex);
	}
	
	return player_id;
    }
    
    public void takeTurn()
    {
	
    }
    
    public int getLatestGame()
    {
	String sql = "SELECT game_id FROM game WHERE status <= num_players " 
		+ "ORDER BY game_id DESC LIMIT 1";	
	ResultSet rs = performQuery(sql);
	int id = 1;

	try 
	{
	    if (!rs.next())
	    {
		return 1;
	    }
	    id = rs.getInt("game_id");
	} 
	catch (SQLException ex) 
	{
	    Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE, 
	    	null, ex);
	}
	
	return id;
    }
    
    public ArrayList<Player> restorePlayers()
    {
	StringBuilder sb = new StringBuilder();
	String sql = "SELECT player_id, player_name FROM player WHERE "
		+ "game_id = " + state.get("game");
	ResultSet rs = performQuery(sql);
	ArrayList<Player> players = null;
	try 
	{
	    players = new ArrayList<Player>();
	    while (rs.next()) 
	    {
		int id = rs.getInt("player_id");
		String name = rs.getString("player_name");
		players.add(new Player(id, name));
	    }
	} 
	catch (SQLException ex) 
	{
	    Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE, 
	    	null, ex);
	}
	
	return players;
    }
    
    private ResultSet performQuery(String sql)
    {
	Statement statement = null;
	ResultSet rs = null;

	try 
	{
	    statement = connection.createStatement();
	    rs = statement.executeQuery(sql);

	} 
	catch (SQLException ex) 
	{
	    Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE, 
	    	null, ex);
	}
	return rs;
    }
    
    private void performUpdate(String sql)
    {
	Statement statement = null;	
	try
	{
	    statement = connection.createStatement();
	    statement.executeUpdate(sql);	    
	}
	catch (SQLException ex) 
	{
	    Logger.getLogger(
	   	PointDatabase.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    // Using PlayerDeck is something to consider.
    public ArrayList<Card> restoreCards()
    {
	StringBuilder sb = new StringBuilder();
	String sql = "SELECT card_id, player_id, card_val FROM cards WHERE " 
		+ "game_id = " + state.get("game") + " AND player_id = " 
		+ state.get("player") + " ORDER by card_id ASC;";
	ResultSet rs = performQuery(sql);
	ArrayList<Card> cards = null;
	try 
	{
	    cards = new ArrayList<Card>();
	    while (rs.next()) 
	    {
		int card_id = rs.getInt("card_id");
		int card_val = rs.getInt("card_val");
		cards.add(new Card(card_val, card_id));
	    }
	} 
	catch (SQLException ex) 
	{
	    Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE, 
	    	null, ex);
	}
	
	return cards;
    }

    public ArrayList<Card> dealReplacementCards() 
    {
	int how_many = (Integer) state.get("num_cards_to_replace");
	int player = (Integer) state.get("player");
	int game = (Integer) state.get("game");
	ArrayList<Card> cards = new ArrayList<Card>();
	StringBuilder ids = new StringBuilder();
	Statement statement = null;

	String sql = "SELECT card_id, card_val FROM cards WHERE game_id = " 
		+ game + " AND player_id = -1 ORDER BY RAND() LIMIT " + how_many;
	ResultSet rs = this.performQuery(sql);

	try 
	{
	    statement = connection.createStatement();
	    while (rs.next())
	    {
		int key = rs.getInt("card_id");
		int val = rs.getInt("card_val");
		Card c = new Card(val, key);
		cards.add(c);
		sql = "UPDATE cards SET player_id = " + player 
			+ " WHERE game_id = " + game + " AND card_id = " + key;
		statement.addBatch(sql);
	    }

	    statement.executeBatch();
	}
	
	catch (SQLException ex) 
	{
	    Logger.getLogger(
	    	PointDatabaseQuery.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	return cards;
    }

    public void discardCards()
    {
	StringBuilder ids = new StringBuilder();
	ArrayList<Card> cards = (ArrayList<Card>) state.get("cards");
	for (Card c : cards)
	{
	    ids.append(c.key());
	    ids.append(",");
	}
	
	String id_str = ids.substring(0, ids.length() - 1);
	
	Statement statement = null;
	try
	{
	    statement = connection.createStatement();
	    String sql = "DELETE FROM cards WHERE card_id IN (" + id_str + ")";
	    System.out.println(sql);
	    statement.executeUpdate(sql);
	} 
	catch (SQLException ex)
	{
	    Logger.getLogger(
	    	PointDatabaseQuery.class.getName()).log(Level.SEVERE, null, ex);
	}	
    }

    public Integer newGame(ArrayList<Card> cards, int game)
    {
	Statement statement = null;
	game++;
	try
	{
	    statement = connection.createStatement();
	    String sql = "INSERT INTO game (num_players, status) VALUES (2, 0)";
	    statement.executeUpdate(sql);

	    int player = -1;

	    for (Card c : cards)
	    {
		sql = "INSERT INTO cards (card_id, game_id, card_val, player_id)"
			 + " VALUES (" + c.key() + ", " + game + ", " + c.value() 
			 	+ ", " + player +  ")";
		statement.addBatch(sql);
	    }
	    statement.executeBatch();
	} 
	catch (SQLException ex)
	{
	    Logger.getLogger(
	    	PointDatabaseQuery.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	return game;
    }
    
    public void addPlayers(ArrayList<Player> players)
    {
	Statement statement = null;
	String sql;
	int num_players = players.size();
	int game = (Integer) state.get("game");
	String name[] = new String[2];
	name[0] = "Jack";
	name[1] = "Nancy";
	try
	{
	    statement = connection.createStatement();
	    for (int i = 0; i < num_players; i++)
	    {
		sql = "INSERT INTO player (game_id, player_id, player_name," 
			+ "score) VALUES (" + game + ", " + i + ", '" 
				+ name[i] + "', 0)";
		statement.addBatch(sql);
	    }
	    statement.executeBatch();
	}
	catch (SQLException ex)
	{
	    Logger.getLogger(
	    	PointDatabaseQuery.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    public void dealCards(int num_players)
    {
	Statement statement = null;
	String sql = null;
	int player_id = 0;
	try
	{
	    statement = connection.createStatement();
	    for (int i = 0; i < num_players * 5; i++)
	    {
		sql = "UPDATE cards SET player_id = " + player_id + " WHERE " 
			+ "player_id = -1 ORDER by RAND() LIMIT 1";
		statement.addBatch(sql);
		player_id++;

		if (player_id == num_players)
		{
		    player_id = 0;
		}
	    }
	    statement.executeBatch();
	}
	catch (SQLException ex)
	{
		Logger.getLogger(
			PointDatabaseQuery.class.getName()).log(
				Level.SEVERE, null, ex);
	}
    }
    
    public void clear()
    {
	Statement statement = null;
	try
	{
	    statement = connection.createStatement();
	    String sql = "TRUNCATE table player";
	    statement.addBatch(sql);
	    sql = "TRUNCATE table cards";
	    statement.addBatch(sql);
	    sql = "TRUNCATE table game";
	    statement.addBatch(sql);
	    statement.addBatch(sql);
	    
	    statement.executeBatch();
	}
	catch (SQLException ex)
	{
		Logger.getLogger(
			PointDatabaseQuery.class.getName()).log(
				Level.SEVERE, null, ex);
	}	
    }
}
