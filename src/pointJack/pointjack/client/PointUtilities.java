package pointJack.pointjack.client;

import pointJack.game.Logic;
import pointJack.pointjack.game.PointsLogic;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class PointUtilities 
{
    public static String intToString(int val)
    {
	if (val == PointsLogic.MULT_2)
	{
	    return "x2";
	}
	if (val == PointsLogic.MULT_3)
	{
	    return "x3";
	}
	if (val == PointsLogic.MULT_4)
	{
	    return "x4";
	}
	if (val == PointsLogic.MULT_5)
	{
	    return "x5";
	}
	return "" + val;
    }
    
    public static int rawInt(int val)
    {
	if (val > PointsLogic.BASE)
	    return val -= PointsLogic.BASE - 1;
	else
	    return val;
    }
    
    public static int stringToInt(String val)
    {
	if (val.equals("x2"))
	{
	    return PointsLogic.MULT_2;
	}
	if (val.equals("x3"))
	{
	    return PointsLogic.MULT_3;
	}
	if (val.equals("x4"))
	{
	    return PointsLogic.MULT_4;
	}
	if (val.equals("x5"))
	{
	    return PointsLogic.MULT_5;
	}
	return Integer.parseInt(val);	
    }
}
