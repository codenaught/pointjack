package pointJack.pointjack.game;

import pointJack.game.Card;
import pointJack.game.Logic;
import pointJack.game.Player;
import pointJack.game.PlayerDeck;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import pointJack.pointjack.client.PointUtilities;

/**
 *
 * PointsLogic contains logical methods instrumental in PointsJack.
 * Important note: The database operations are decoupled from Logic.
 * All operations performed in Logic are local operations that may or may not
 *	be updated server side outside of the scope of this class.
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class PointsLogic implements Logic
{

    private static PointsLogic instance = null;
   
    public static final int BASE = 10;
    public static final int MULT_2 = BASE + 1;
    public static final int MULT_3 = BASE + 2;
    public static final int MULT_4 = BASE + 3;
    public static final int MULT_5 = BASE + 4;
    public static final int NUM_PLAYERS = 2;
   
    private int num_players;
    private ArrayList<Player> players;
    private int num_cards;
    private int gameID;
    private int pointsToWin;    
    private Player currentPlayer;
    private Player user;
    private Player winner;
    
    protected PointsLogic()
    {
	players = new ArrayList<Player>(NUM_PLAYERS);
	currentPlayer = null;
	num_cards = 0;
	num_players = 0;
	user = null;
	pointsToWin = 200;
    }

   public static PointsLogic getInstance() 
   {
      if(instance == null) 
      {
	  //System.out.println("Making instance of Logic");
         instance = new PointsLogic();
      }
      return instance;
   }
   
    @Override
    public Player getWinner()
    {
       return winner;
    }
   
    @Override
    public void setWinner(int id)
    {
       winner = players.get(id);
    }

    public int getGameID() 
    {
	return gameID;
    }

    public void setGameID(int gameID) 
    {
	this.gameID = gameID;
    }
    
    @Override
    public ArrayList<Player> getPlayers()
    {
	return players;
    }
    
    @Override
    public void setUserPlayer(Player p)
    {
	user = p;
    }
    
    @Override
    public Player getUserPlayer()
    {
	return user;
    }
    
    @Override
    public int getNumPlayers()
    {
	return num_players;
    }
    
    @Override
    public void addPlayer(Player player)
    {	
	players.add(player);
	num_players++;
    }
    
    public boolean isValidMove()
    {
	return true;
    }
    
    @Override
    public int processTurn()
    {
	// Grab the current player's played cards.
	PlayerDeck cards = currentPlayer.getActiveCards();
	int points = 0;
	boolean times = false;
	PlayerDeck temp = new PlayerDeck(cards.size());
	
	for (Card c : cards)
	{
	    if (isMultiplier(c.value()))
	    {
		times = true;
	    }
	    try 
	    {
		temp.add(c);
	    } 
	    catch (Exception ex) 
	    {
		Logger.getLogger(PointsLogic.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	
	for (Card c : temp)
	{
	    if (points == 0 || !times)
	    {
		points += PointUtilities.rawInt(c.value());
	    }
	    else if (times)
	    {
		points *= PointUtilities.rawInt(c.value());
	    }
	}

	//System.out.println("total points played: " + points);
	return points;
    }
    
    public boolean isMultiplier(int val)
    {
	return (val == MULT_2 ||val == MULT_3 || val == MULT_4 || val == MULT_5);
    }
    
    @Override
    public void addCardToUserPlayingDeck(Card c)
    {
	try 
	{
	    currentPlayer.getActiveCards().add(c);
	    //System.out.println("user is about to play: " + user.getActiveCards()[0] + " and " + user.getActiveCards()[1]);
	} 
	catch (Exception ex) 
	{
	    Logger.getLogger(PointsLogic.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    @Override
    public String toString()
    {
	String s = "";
	for(Player p : players)
	{
	    if (p != null)
		s += p.toString() + "\n";
	}
	
	return s;
    }

    @Override
    public void setCurrentPlayer(int id) 
    {
	if (id >= 0 && id < this.getNumPlayers())
	{
	    currentPlayer = players.get(id);
	}
    }
    
    @Override
    public boolean isYourTurn()
    {
	if (user != null && currentPlayer != null)
	{
	    return user.getID() == currentPlayer.getID();
	}
	return false;
    }

    @Override
    public void removeFromUserDeck(Card c)
    {
	currentPlayer.getDeck().removeCard(c);
    }

    @Override
    public void setPointsToWin(int points) 
    {
	pointsToWin = points;
    }
    
    @Override
    public int getPointsToWin()
    {
	return pointsToWin;
    }

    @Override
    public void restart() 
    {
	players = new ArrayList<Player>();
	num_players = 0;
	winner = null;
    }
    
    public void getScores()
    {
	
    }

    @Override
    public void updateScore(Player p, int pointsToAdd)
    {
	p.getScore().add("pointjack", pointsToAdd);
    }

    @Override
    public void setPlayers(ArrayList<Player> players) 
    {
	this.players = players;
    }
}
