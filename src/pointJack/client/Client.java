package pointJack.client;

import pointJack.game.Logic;
import java.util.HashMap;
import pointJack.pointjack.client.PointDatabase;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public interface Client 
{
    boolean newGame();
    void restore();
    void setLogic(Logic logic);
    Logic getLogic();
    String sendCommand();
    HashMap<String, Object> getState();
    void setOver();
    void setDatabase(PointDatabase db);
    PointDatabase getDatabase();
}
