package pointJack.client;

import org.ini4j.Ini;

import pointJack.pointjack.gui.listeners.GameListener;
import pointJack.pointjack.client.PointClient;
import pointJack.pointjack.game.PointsLogic;
import pointJack.pointjack.gui.PointGUI;
import pointJack.pointjack.client.PointDatabase;

import pointJack.game.Logic;

import java.awt.event.ActionListener;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;

import javax.swing.Timer;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class RunPointsGame
{
	public static void main(String args[])
	{
		// Logic is a singleton instance, no need to have multiple instances at the same time.
		Logic logic = PointsLogic.getInstance();
		PointDatabase db;

		String url, db_path = "", db_pass = "", db_user = "";
		boolean debug = false;
		int refresh_rate = 0;
		HashMap<String, String> argMap = new HashMap<String, String>();

		// Some constants.
		final String RESET_STR = "reset";
		final int DEFAULT_REFRESH_TIME = 1200;
		final String CONTACT_NAME = "Jack";

		Logger.getLogger(PointClient.class.getName()).setLevel(Level.ALL);

		// For legacy compatibility, to be removed soon.
		// Note the PHP script shouldn't be used anymore.
		url = "http://localhost/game/script.php";
		Ini ini = null;

		for (String arg : args)
		{
			parseArgument(argMap, arg);
		}
		try
		{
			ini = new Ini(new File("settings.ini"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		if (!ini.isEmpty())
		{
			db_user = ini.get("database", "user");
			db_pass = ini.get("database", "pass");
			db_path = ini.get("database", "path");

			debug = ini.get("general", "debug", boolean.class);

			refresh_rate = ini.get("gui", "refresh_rate", int.class);
		}

		// Overwrites...
		if (argMap.containsKey("db_path"))
			db_path = argMap.get("db_path");
		if (argMap.containsKey("db_pass"))
			db_pass = argMap.get("db_pass");
		if (argMap.containsKey("db_user"))
			db_user = argMap.get("db_user");

		// Update the refresh rate if it has not already been set in the ini file and it exists in arguments.
		refresh_rate = refresh_rate == 0 && argMap.containsKey("refresh") ?
				Integer.parseInt(argMap.get("refresh")) : DEFAULT_REFRESH_TIME;
		// Update false debug to true if it exists in the arguments.
		if (!debug)
			debug = argMap.containsKey("debug");

		// For my own lazy usage.
		// Note: I should just use a custom ant file just for myself with these values in it.
		if (debug && !argMap.containsKey("noauto"))
		{
			db_user = "root";
			db_pass = "";
			db_path = "localhost/pointgame";
		}

		Client c = new PointClient(url);
		c.setLogic(logic);

		db = new PointDatabase(db_user, db_pass, db_path);
		db.setState(c.getState());
		db.setLogic(c.getLogic());
		// Try to connect to the database.
		try
		{
			db.connect();
		}
		// An error here is severe, exit the program gracefully.
		catch (java.sql.SQLException ex)
		{
			if (debug)
			{
				Logger.getLogger(PointDatabase.class.getName()).log(Level.SEVERE,
						null, ex);
			}
			else
			{
				System.err.println("Had some trouble connecting to the database. Please contact "
						+ CONTACT_NAME);
			}

			System.exit(1);
		}

		c.setDatabase(db);

		if (argMap.containsKey(RESET_STR))
		{
			db.query("clear");
		}

		c.newGame();
		c.restore();

		pointJack.gui.GUI gui = new PointGUI(c);

		ActionListener g = new GameListener(gui);
		Timer time = new Timer(refresh_rate, g);

		time.start();
	}

	/*  Parses a string (intended to be from command line).
	 *  Can recognize both key=val form and variable_set form, where
	 *      variable_set indicates that the value is set and is tested for existence.
	 */
	public static void parseArgument(HashMap<String, String> argMap, String arg)
	{
		// Grab the part up to the but not including the = sign.
		int index = arg.indexOf('=');

		// No equals means we are setting some setting to true.
		if (index == -1)
		{
			argMap.put(arg.toLowerCase(), "1");
			return;
		}

		// Cannot allow an arg beginning with an '='.
		if (index == 0)
		{
			return;
		}

		String key = arg.substring(0, index);
		String val = arg.substring(index + 1);

		argMap.put(key.toLowerCase(), val.toLowerCase());
	}
}