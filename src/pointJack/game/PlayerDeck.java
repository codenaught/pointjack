package pointJack.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class PlayerDeck implements Iterable<Card>
{
    private ArrayList<Card> deck;

    private int max_size;
    private int num_cards;
    
    public PlayerDeck(int max)
    {
	deck = new ArrayList<Card>(max_size);
	max_size = max;
	num_cards = 0;
    }
    
    public Card get(int key)
    {
	for (Card c : deck)
	{
	    if (c.key() == key)
	    {
		return c;
	    }
	}
	return null;
    }
    
    public ArrayList<Card> getDeck()
    {
	return deck;
    }
    
    public Card getByIndex(int index)
    {
	if (index >= deck.size())
	{
	    return deck.get(index);
	}
	return null;
    }
    
    public void removeCard(Card c)
    {
	deck.remove(c);
	num_cards--;
    }
    
    public Card remove(int key)
    {
	Card c = null;
	for (int i = 0; i < num_cards; i++)
	{
	    if (deck.get(i).key() == key)
	    {
		num_cards--;
		c = deck.remove(i);
		break;
	    }
	}
	return c;
    }
    
    public void removeValueOnce(int value)
    {
	int key = -1;
	for (Card c : deck)
	{
	    if (c.value() == value)
		key = c.key();
	}
	if (key != -1)
	{
	    this.remove(key);
	}
    }
    
    public void replaceEmptyCard(Card c) 
    {
	int key = -1;
	for (Card temp : deck)
	{
	    if (temp.value() == 0)
	    {
		key = temp.key();
	    }
	    
	}
	this.remove(key);
	try {
	    this.add(c);
	} catch (Exception ex) {
	    Logger.getLogger(PlayerDeck.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    public void add(Card c) throws Exception
    {
	if (num_cards > max_size)
	    throw new Exception("Too many cards in deck");
	deck.add(c);
	num_cards++;
    }
    
    public void clear()
    {
	deck.clear();
	num_cards = 0;
    }
    
    public int size()
    {
	return num_cards;
    }

    @Override
    public Iterator iterator() 
    {
	return new DeckIterator();
    }
    
    private class DeckIterator implements Iterator
    {
	private int index;
	
	public DeckIterator()
	{
	    index = 0;
	}
	
	@Override
	public boolean hasNext() 
	{
	    if (index < deck.size())
	    {
		return true;
	    }
	    else
	    {
		return false;
	    }
	}

	@Override
	public Card next() {
	    Card c = deck.get(index);
	    index++;
	    return c;
	}

	@Override
	public void remove() {
	    
	}
    }
}
