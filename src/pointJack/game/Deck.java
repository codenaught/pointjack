package pointJack.game;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD

 */
public class Deck 
{
    private int size;
    private Deque<Card> deck;
    
    public Deck(int num_cards)
    {
	deck = new ArrayDeque<Card>(num_cards);
	size = num_cards;
	for (int i = 0; i < num_cards; i++)
	{
	    deck.add(new Card(0, i));
	}
    }
    
    public void clear()
    {
	deck.clear();
    }
    
    public void add(Card c)
    {
	deck.add(c);
	size++;
    }
    
    public Card peekNext()
    {
	Card c = deck.peekFirst();
	return c;
    }
    
    public Card retrieveNext()
    {
	Card c = deck.removeFirst();
	return c;
    }
    
    public int getSize()
    {
	return size;
    }
}
