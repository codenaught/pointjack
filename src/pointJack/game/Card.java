package pointJack.game;

/**
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 * @author jthor
 */
public class Card
{
    private int value;
    private int key; // Every card must have a unique ID.
    
    public Card(int val, int key)
    {
	value = val;
	this.key = key;
    }
    
    public int value()
    {
	return value;
    }
    
    public int key()
    {
	return key;
    }
    
    @Override
    public boolean equals(Object obj)
    {
	if (obj == this)
	    return true;
	if (obj == null || !(obj instanceof Card))
	    return false;
	Card other = (Card) obj;
	return other.value == this.value &&
		other.key == this.key;
    }

    @Override
    public int hashCode() 
    {
	int hash = 7;
	hash = 89 * hash + this.value;
	return hash;
    }
    
    @Override
    public String toString()
    {
	return "(" + key + ") " + value + "";
    }
}
