package pointJack.game;

import java.util.ArrayList;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public interface Logic 
{
    boolean isYourTurn();
    Player getUserPlayer();
    Player getWinner();
    void setWinner(int idWinner);
    int getPointsToWin();
    int getNumPlayers();
    void addCardToUserPlayingDeck(Card card);
    void removeFromUserDeck(Card card);
    void setCurrentPlayer(int playerID);
    void restart();
    int processTurn();
    ArrayList<Player> getPlayers();
    void setPlayers(ArrayList<Player> players);
    void setUserPlayer(Player p);
    void addPlayer(Player p);
    void setPointsToWin(int number);

    public void updateScore(Player p, int pointsToAdd);
}
