package pointJack.game;

import java.util.HashMap;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class PlayerScore implements Score
{
    private HashMap<String, Double> stat;
    
    public PlayerScore()
    {
	stat = new HashMap<String, Double>();
	stat.put("pointjack", 0.0);
    }

    @Override
    public double get(String key) 
    {
	return stat.get(key);
    }

    @Override
    public double add(String key, double val) 
    {
	Double d = stat.get(key) + val;
	stat.put(key, d);
	return d;
    }

    @Override
    public double subtract(String key, double val) 
    {
	Double d = stat.get(key) - val;
	stat.put(key, d);
	return d;	
    }

    @Override
    public void set(String key, double val) 
    {
	stat.put(key, val);	
    }
}
