package pointJack.game;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public interface Score 
{
    double get(String key);
    double add(String key, double val);
    void set(String key, double val);
    double subtract(String key, double val);
}
