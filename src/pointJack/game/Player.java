package pointJack.game;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class Player 
{
    private PlayerDeck deck;
    // Keys currently selected in the player's deck.
    private PlayerDeck activeDeck;

    private Score score;
    private int id;
    private String name;
    
    public Player(int id)
    {
	score = new PlayerScore();
	activeDeck = new PlayerDeck(2);
	deck = new PlayerDeck(5);
	this.id = id;
    }

    public Player(int id, String name) 
    {
	score = new PlayerScore();
	activeDeck = new PlayerDeck(5);
	deck = new PlayerDeck(5);
	this.id = id;
	this.name = name;
    }
    
    public int getID()
    {
	return id;
    }
    
    @Override
    public String toString()
    {
	String s = id + " has: [";
	for (Card c : deck)
	{
	    s += c.value() + ", ";
	}
	
	s += "]";
	
	return s;
    }

    public PlayerDeck getActiveCards() 
    {
	return activeDeck;
    }
    
    public PlayerDeck getDeck()
    {
	return deck;
    }
    
    public void playCards(PlayerDeck c)
    {
	activeDeck = c;
    }

    public String getName() 
    {
	return name;
    }

    public Score getScore() 
    {
	return score;
    }
}
