
package server;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server 
{
    private ServerSocket serverSock;
    private Socket socket;
    private String host;
    private int port;
    private BufferedReader input;

    public Server(String host, int port)
    {
	this.host = host;
	this.port = port;
    }
    
    public void start()
    {
	try 
	{
	    serverSock = new ServerSocket(port);
	    serverSock.bind(new InetSocketAddress(host, port));
	    while (true)
	    {
		socket = serverSock.accept();
		PrintWriter pw = new PrintWriter(socket.getOutputStream());
		
		Socket s = new Socket(host, port);
		InputStream in = s.getInputStream();
		BufferedReader read = new BufferedReader(new InputStreamReader(in));
		
		System.out.println(read.readLine());
	    }
	} 
	catch (UnknownHostException ex) 
	{
	    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
	    System.exit(1);
	} 
	catch (IOException ex) 
	{
	    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
	    System.exit(1);
	}
    }
    
    public void close()
    {
	
    }
    
}
