package pointJack.gui;

import pointJack.client.Client;
import pointJack.game.Logic;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class GamePanel extends JPanel
{
    private static final long serialVersionUID = 1L;
    protected JLabel description;
    protected Logic logic;
    protected Client client;
    protected GUI gui;
    
    public GamePanel(GUI gui)
    {
	construct(gui);
	this.setLayout(new GridBagLayout());
    }
    
    public GamePanel(GUI gui, LayoutManager layout)
    {
	construct(gui);
	this.setLayout(layout);
    }
    
    private void construct(GUI gui)
    {
	this.gui = gui;
	client = this.gui.getClient();
	logic = client.getLogic();	
    }

    public void refresh()
    {

    }
    
    public Client getClient() 
    {
	return client;
    }

    public void setClient(Client client) 
    {
	this.client = client;
    }

    public JLabel getDescription() 
    {
	return description;
    }

    public void setDescription(JLabel description) 
    {
	this.description = description;
    }

    public GUI getGui() 
    {
	return gui;
    }

    public void setGui(GUI gui) 
    {
	this.gui = gui;
    }

    public Logic getLogic() 
    {
	return logic;
    }

    public void setLogic(Logic logic) 
    {
	this.logic = logic;
    }
}
