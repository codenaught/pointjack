package pointJack.gui;

import pointJack.game.Player;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class ScorePanel extends GamePanel
{
    private static final long serialVersionUID = 1L;
    private final JLabel field;
    private String[] names;

    public ScorePanel(GUI gui)
    {
	super(gui);
	field = new JLabel();
	Color x = new Color(0xC6DEFF);
	this.setBackground(x);
	this.setBorder(BorderFactory.createLineBorder(new Color(0x70a1c3), 1));
	//String scoreText = 
	names = new String[logic.getNumPlayers()];
	int i = 0;
	for (Player p : logic.getPlayers())
	{
	    names[i] = p.getName();
	    i++;
	}
	
	StringBuilder builder = new StringBuilder();

	builder.append("<html><h1 style='margin: 1px; padding: 0;'><em>Now Playing:</em>");	
	builder.append("</h1><h1 style='margin: 1px; padding: 0;'><em>Score:</em></h1>");
	field.setText(builder.toString());
	this.add(field);
	this.repaint();

    }
    
    @Override
    public void refresh()
    {
	int id = logic.getUserPlayer().getID();
	String txt = "";
	StringBuilder builder = new StringBuilder();
	JFormattedTextField nowplaying = new JFormattedTextField();


	if (logic.getWinner() != null)
	{
		txt = "<html><h1 style='margin: 1px; padding: 0;'>" + logic.getWinner().getName() + " has won!</h1></html>";
		field.setText(txt);
	}	
	else
	{
	    if (id < logic.getNumPlayers())
	    {
		Player p = logic.getPlayers().get(id);
		
		builder.append("<html><h1 style='margin: 1px; padding: 0;'><em>Now Playing:</em> ");
		builder.append(p.getName());
		builder.append("</h1><h1 style='margin: 1px; padding: 0;'><em>Score:</em> ");
		nowplaying.setText(builder.toString());

	    }
	    client.getDatabase().query("getscores");
	    //client.getState().put("action", "getscores");
	    	    
	    //String s = client.sendCommand();
	    //StringTokenizer st = new StringTokenizer(s, "<br />");

	    for (int i = 0; i < logic.getNumPlayers(); i++)
	    {
		Player p = logic.getPlayers().get(i);
		double score = p.getScore().get("pointjack");
		/*
		String score = st.nextToken();
		 */
		//p.getScore().set("main", Double.parseDouble(score));
		builder.append(names[i]);
		builder.append(": ");
		builder.append((int)score);
		builder.append(" ");
		if (score >= logic.getPointsToWin())
		{
		    logic.setWinner(i);
		    client.setOver();
		    gui.setNewGame(true);
		}
	    }
	    builder.append("</h1></html>");
	    txt = builder.toString();
	    field.setText(txt);

	}

	this.removeAll();
	this.add(field);
	//this.add(nowplaying);
	this.repaint();
    }
}
