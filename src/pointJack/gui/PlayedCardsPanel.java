package pointJack.gui;

import pointJack.game.Card;
import pointJack.game.PlayerDeck;

import java.util.logging.Level;
import java.util.logging.Logger;

import pointJack.pointjack.gui.listeners.TurnMadeListener;
import pointJack.pointjack.client.PointUtilities;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * This GamePanel is responsible for the cards being played.
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class PlayedCardsPanel extends GamePanel 
{
    private static final long serialVersionUID = 1L;
    
    private PlayerDeck cards;
    private final JButton submit;
    private JButton[] buttons;
    private GridBagConstraints constraints;
    private final JButton reset;

    public PlayedCardsPanel(GUI gui)
    {
	super(gui);
	this.setLayout(new GridBagLayout());
	cards = logic.getUserPlayer().getActiveCards();
	
	constraints = new GridBagConstraints();
	constraints.ipadx = 40;
	constraints.ipady = 100;
	constraints.insets = new Insets(10, 10, 10, 10);
	
	submit = new JButton("Play Selected Cards");
	description = new JLabel("<html><h3 style='color: white;'>Cards To Play:</h3></html>");
	this.add(description, constraints);
	buttons = new JButton[2];

	for (int i = 0; i < 2; i++)
	{
	    buttons[i] = new JButton();
	    buttons[i].setEnabled(false);
	    this.add(buttons[i], constraints);
	}

	reset = new JButton("Undo");

	this.add(submit);
	this.add(reset);
	Color x = new Color(0xa7b7b7);
	this.setBackground(x);
	this.setBorder(BorderFactory.createLineBorder(new Color(0x70a1c3), 1));
	reset.setEnabled(false);
	submit.setEnabled(false);
	TurnMadeListener tml = new TurnMadeListener(gui);
	reset.addActionListener(tml);

	
	submit.addActionListener(tml);
    }
    
    /*
     * Refresh all of the contents of the cards being played.
     * TODO: Introduce detection that prevents updating when
     *	    no change in played cards is made.
     */
    @Override
    public void refresh()
    {
	Logger.getLogger(PlayedCardsPanel.class.getName()).log(Level.SEVERE, null, "");
	this.removeAll();
	this.add(description, constraints);
	
	int index = 0;
	
	if (cards.size() == 0)
	{
	    reset.setEnabled(false);
	    submit.setEnabled(false);
	}
	for (Card c : cards)
	{	    
	    String buttonStr = "Err";
	    buttonStr = PointUtilities.intToString(c.value());
	    buttons[index].setText(buttonStr);
	    this.add(buttons[index], constraints);
	    index++;
	}

	// TODO: Remove hardcoded number.
	if (cards.size() < 2)
	{
	    for (int i = cards.size(); i < 2; i++)
	    {
		String buttonStr = "";
		buttons[index].setText(buttonStr);
		this.add(buttons[index], constraints);
		index++;		
	    }
	}

	this.add(submit);
	this.add(reset);
	
	this.repaint();
	this.setVisible(true);
    }
    
    /*
     * Adds the card specified to the played cards area.
     */
    public void placeCard(Card c)
    {
	try 
	{
	    cards.add(c);
	}
	catch (Exception ex) 
	{
	    Logger.getLogger(PlayedCardsPanel.class.getName()).log(Level.SEVERE, null, ex);
	}
	
	reset.setEnabled(true);
	submit.setEnabled(true);
    }

    public void reset() 
    {
	cards.clear();
    }
}
