package pointJack.gui;

import javax.swing.ImageIcon;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class ImageResourceManager
{
    private static ImageResourceManager instance = null;
    private ImageIcon[] icons;

    protected ImageResourceManager()
    {
	icons = new ImageIcon[15];
	for (int i = 0; i < 15; i++)
	{
	    icons[i] = new ImageIcon("cards/" + i + ".png");
	}
    }

   public static ImageResourceManager getInstance() 
   {
      if(instance == null) 
      {
         instance = new ImageResourceManager();
      }
      return instance;
   }
   
   public ImageIcon getImage(int id)
   {
       return icons[id];
   }
}
