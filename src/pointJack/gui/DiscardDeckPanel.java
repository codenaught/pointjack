package pointJack.gui;

import pointJack.game.Card;
import pointJack.game.PlayerDeck;
import java.util.logging.Level;
import java.util.logging.Logger;
import pointJack.pointjack.client.PointUtilities;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public class DiscardDeckPanel extends GamePanel
{
    private static final long serialVersionUID = 1L;
    private int[] vals;
    private PlayerDeck cards;
    private GridBagConstraints constraints;
    private JButton[] discarded;
    private int disIndex;
    private GUI gui;

    public DiscardDeckPanel(GUI gui)
    {
	super(gui);
	this.gui = gui;
	this.setLayout(new GridBagLayout());
	constraints = new GridBagConstraints();
	constraints.ipadx = 40;
	constraints.ipady = 100;
	constraints.insets = new Insets(10, 10, 10, 10);
	description = new JLabel("<html><h3 style='color: white;'>Last Cards Played:</h3>");
	Color x = new Color(0xa7b7b7);
	//Color x = Color.cyan;
	this.setBorder(BorderFactory.createLineBorder(new Color(0x70a1c3), 1));
	this.setBackground(x);
	
	cards = new PlayerDeck(2);
	discarded = new JButton[2];
	disIndex = 0;
	this.add(description, constraints);

	// TODO Removed hardcoded stuff.
	for (int i = 0; i < 2; i++)
	{
	    JButton b = new JButton("?");
	    b.setEnabled(false);
	    b.setText("?");
	    discarded[i] = b;
	    this.add(discarded[i], constraints);
	}
	vals = new int[2];

    }
    
    @Override
    public void refresh()
    {
	this.removeAll();
	this.add(description, constraints);
	int index = 0;
	
	for (Card c : cards)
	{
	    String s = PointUtilities.intToString(c.value());
	    discarded[index].setText(s);
	    this.add(discarded[index], constraints);
	    index++;
	}

	this.repaint();
    }
    
    public PlayerDeck getCards()
    {
	return cards;
    }
    
    public void setDiscarded(Card c)
    {
	try
	{
	    cards.add(c);
	} 
	catch (Exception ex) 
	{
	    Logger.getLogger(DiscardDeckPanel.class.getName()).log(Level.SEVERE, null, ex);
	}	
    }
}
