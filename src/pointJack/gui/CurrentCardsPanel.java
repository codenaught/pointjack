package pointJack.gui;

import pointJack.game.Card;
import pointJack.game.PlayerDeck;

import java.util.logging.Level;
import java.util.logging.Logger;

import pointJack.pointjack.gui.listeners.CurrentCardsListener;
import pointJack.pointjack.client.PointUtilities;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 * The CurrentCardsPanel is a GamePanel that shows the current hand of the player.
 */
public class CurrentCardsPanel extends GamePanel
{
    private static final long serialVersionUID = 1L;
    private final CurrentCardsListener ccl;
    private PlayerDeck cards;
    
    // The cards, implemented as buttons.
    private JButton[] buttons;
    //private Deck deck;
    private GridBagConstraints constraints;

    public CurrentCardsPanel(GUI gui)
    {
	super(gui);

	constraints = new GridBagConstraints();
	constraints.ipadx = 40;
	constraints.ipady = 100;
	constraints.insets = new Insets(10, 10, 10, 10);

	// TO DO: Rework this crap.
	description = new JLabel("<html><h3 style='color: black;'>Your Deck:</h3></html>");
	this.add(description, constraints);

	Color x = new Color(0xC6DEFF);	
	this.setBackground(x);

	this.setBorder(BorderFactory.createLineBorder(new Color(0x70a1c3), 1));
	
	// TO DO: HARD NUMBER CODED YIKES!
	//cards = new PlayerDeck(5);
	cards = logic.getUserPlayer().getDeck();
	buttons = new JButton[5];
	//deck = new Deck(5);
	
	// Create the listener.
	ccl = new CurrentCardsListener(gui);

	// Add the buttons.
	for (int i = 0; i < 5; i++)
	{
	    try 
	    {
		// TODO Remove hardcoded 0.
		buttons[i] = new JButton("0");
		buttons[i].setEnabled(false);
		//cards.add(new Card(0, i));
		this.add(buttons[i], constraints);
	    } 
	    catch (Exception ex) 
	    {
		Logger.getLogger(CurrentCardsPanel.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	
	// Add Listener.
	for(JButton j : buttons)
	{
	    j.addActionListener(ccl);
	    j.setVisible(true);
	}
    }

    /*
     * Handles the operation of rebuilding the panel of cards to play, reflective
     *	    of changes made as the game is being played (new cards being played).
     *	    TODO: Only refresh when really needed.
     */
    @Override
    public void refresh() 
    {
	boolean yourTurn = logic.isYourTurn();
	//boolean redraw = true;
	if (logic.getUserPlayer() != null)
	{	
	   // if (redraw)
	    {
		this.removeAll();
		this.add(description, constraints);
		cards = logic.getUserPlayer().getDeck();
		int activeCount = logic.getUserPlayer().getActiveCards().size();
		System.out.println("Active count: " + activeCount);
		int index = 0;
		for (Card c : cards)
		{
		    if (c.value() == 0)
		    {
			buttons[index].setText("X");
			buttons[index].setEnabled(false);
		    }
		    else
		    {
			//ImageIcon i = new ImageIcon("card.png");
			buttons[index].setText(PointUtilities.intToString(c.value()));
			buttons[index].putClientProperty("id", c.key());
			
			// TODO Remove hardcoded limit.">
			if (activeCount >= 2 || !yourTurn)
			{
			    buttons[index].setEnabled(false);
			}
			else
			{
			    buttons[index].setEnabled(true);   
			}		
		    }
		    this.add(buttons[index], constraints);
		    index++;
		}
	    }
	}
    }
}
