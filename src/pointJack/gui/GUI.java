package pointJack.gui;

import pointJack.client.Client;

/**
 *
 * @author jthor
 * @license http://jthor.no-ip.org/projects/pointjack/license BSD
 */
public interface GUI 
{
    void refresh();
    Client getClient();
    void setNewGame(boolean isNew);   
}
